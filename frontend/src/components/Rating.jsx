// import PropTypes from 'prop-types';
import StarIcon from '@mui/icons-material/Star';
import StarBorderIcon from '@mui/icons-material/StarBorder';
import StarHalfIcon from '@mui/icons-material/StarHalf';

const Rating = ({ value, text, color }) => {
  return (
    <div className="rating">
      <span>
        {value >= 1 ? (
          <StarIcon sx={{ color, fontSize: 18 }} />
        ) : value >= 0.5 ? (
          <StarHalfIcon sx={{ color, fontSize: 18 }} fontSize="small" />
        ) : (
          <StarBorderIcon sx={{ color, fontSize: 18 }} fontSize="small" />
        )}
      </span>
      <span>
        {value >= 2 ? (
          <StarIcon sx={{ color, fontSize: 18 }} fontSize="small" />
        ) : value >= 1.5 ? (
          <StarHalfIcon sx={{ color, fontSize: 18 }} fontSize="small" />
        ) : (
          <StarBorderIcon sx={{ color, fontSize: 18 }} fontSize="small" />
        )}
      </span>
      <span>
        {value >= 3 ? (
          <StarIcon sx={{ color, fontSize: 18 }} fontSize="small" />
        ) : value >= 2.5 ? (
          <StarHalfIcon sx={{ color, fontSize: 18 }} fontSize="small" />
        ) : (
          <StarBorderIcon sx={{ color, fontSize: 18 }} fontSize="small" />
        )}
      </span>
      <span>
        {value >= 4 ? (
          <StarIcon sx={{ color, fontSize: 18 }} fontSize="small" />
        ) : value >= 3.5 ? (
          <StarHalfIcon sx={{ color, fontSize: 18 }} fontSize="small" />
        ) : (
          <StarBorderIcon sx={{ color, fontSize: 18 }} fontSize="small" />
        )}
      </span>
      <span>
        {value >= 5 ? (
          <StarIcon sx={{ color, fontSize: 18 }} fontSize="small" />
        ) : value >= 4.5 ? (
          <StarHalfIcon sx={{ color, fontSize: 18 }} fontSize="small" />
        ) : (
          <StarBorderIcon sx={{ color, fontSize: 18 }} fontSize="small" />
        )}
      </span>
      <div>
        <span>{text && text}</span>
      </div>
    </div>
  );
};

Rating.defaultProps = {
  color: '#f8e825',
};

// Rating.propTypes = {
//   value: PropTypes.number.isRequired,
//   text: PropTypes.string.isRequired,
//   color: PropTypes.string,
// };

export default Rating;
